render one language in another language
nations unified by shared interests, history or institutions
convert into absorbable substances, (as if) with heat or chemical process
devote or adapt exclusively to an skill, study, or work
elevated wooden porch of a house
either half of an archery bow
a removable device that is an accessory to larger object
restrict or confine
orient, be positioned
Bring back to life, return from the dead
anything illusory, not concrete or real
a name or trait that identifies a product or type of thing
the processing or operation of something
line, cover
determine a standard; estimate a capacity or measurement
an interpretation, an account or reading of an event or situation
festive social event, celebration
Make fine adjustments to for optimal    operation; adjust to a certain scale.
a short lyric or poem intended to be sung
effectively wield or manipulate a concrete entity
employ or become employed in a position
group of people defined by a specific profession
storage cabinet furniture
Signal (approval or interest) by winking; briefly shut one eye.
capital stock in a corporation
prove or corroborate
(the content of) a communication.
a partial denture
move to a lower position, terminate something
flow, as of liquids
seal, insulate or protect
scar physically; create a mark on someone or something
Surround completely; close in, as if with a    covering or border.
the act or event of ending a process, state, situation
the price of a good or service
depict or portray in art, words, or model
travel in or be shaped like a zigzag
Annoy, harass, or mock, perhaps playfully.
(Cause to) smell bad.
social ranking or position
ponder, consider, observe carefully
persuade or achieve acceptance
physical matter left behind after a removal process
any tall, vertical shape
physically inflate
heavy radioactive metallic element, atomic number 92
Complain about something or say mean things    to someone.
care about, be bothered by, be concerned by
the tallied points of a game, at a given time
edit so as to block the truth; prevent distribution
a casual, brief meeting with someone
an electrical connection between conductor and earth
an agent's act of terminating an activity or event
notable or famous persons (real or fictional)
a person who manipulate or controls a device or machine
find, determine the place of
to engage in plotting or activities of an illegal or deceitful nature
be equivalent, have the same effect as something
(cause to) survive
the money
a sudden emotional vocalization
go back, restore, revive
settlement of people distant from their homeland
(The discovery or exhibition of) factual evidence that establishes the truth of something.
enter a realm, become involved with
a project or undertaking
a standardized system of measurements for heaviness
a concern or affair
have a strong sexual attraction to
a surrogate expression, utterance for a taboo word
facility for secure storage of things, often money
emit or cause to emit noise
make physical contact with, possibly with the effect of physically manipulating.
find out or be informed of a new information, understand or realize
head of a country
Touch with the lips.
a natural physical force of drawing towards something
a change which is a decrease
an evaluation, an estimation of worth, a grade
having responsibility for managing or supervising
fail or suffer failure
an abundance of something
An authoritative text or the corresponding citation.
An instance of visual perception.
fluctuate, move back and forth
the alleviation of distress
A ceremony marking admission into a religious community.
a social gesture of deferential greeting
move to a new location, state, or situation
Change one for another
a full stop, as a punctuation mark
something that is craved
to grow rapidly
Speak in a high-pitched tone of voice, as          either an animal or human might.
domesticated animals kept for use or profit
a discipline, a branch of knowledge
a person who is aggressively pursued, chased
personnel casualty
escape or avoid a situation or responsibility
a relative loss in price, value
a measurable or quantitative decrease
Change the key, pitch, tone, or volume of,    likely for aesthetic reasons.
a written message of nonacceptance
(cause to) stop speaking or bring about silence
injure, scratch the surface
A plant structure near the top of the plant.
a written list of particular things being offered
a notational mark used in writing, diacritic
the state of believing in, or having confidence in, something or someone
an attempt or effort to achieve a goal
increase in amount, extent, or intensity
clean, wash thoroughly
compress out of shape or into small pieces
point or direct object, weapon, or blow at something or someone
employ or utilize, put into service
Spin, wind, braid, or twist together,    perhaps as when performing lacework.
a decorative container, often for holding flowers
preserve, keep in safety, use frugally
an open motorboat used for transport
a process of development, increase or advancement
Make sure of.
supply or furnish with something needed or wanted
a sharp bladed instrument for shaving off hair
learn, assimilate a body of knowledge
support; hold up; nurture or provide for
keep away, prevent from happening, or not do something
Something that provides advice.
The act of copying the actions of someone else.
a physical entity
lessen in force, impact, or effect
a judgement or opinion about something
flog or whip
act, form, or progress according to a plan or ideal
Germinate, as in a plant.
social sphere, scope of activity or interest
a static vertical layout, e.g. text or numbers
recognize, be familar or well-versed with
a portrayal of someone or something, through words or behavior
a device to control the rate of some activity, e.g., chemical or mechanical
Form an aesthetic or conceptual whole.
human legs, informal usage
goal or objective
protect from heat, cold, or other detrimental conditions
an allotment or percentage of something
something that attains victory
line up or make parallel; bring into proper adjustment
a home furnishing that is a fabric cover
meshed frame over windows and doors
a measure of the likelihood or probability of an event
Anyone who judges or finds fault with something.
the rescue or release of someone, something
observe, perceive
limit, keep under control, deter, forbid
stick out, jut out, protrude
to act as the host of
put an end to, make invalid a process that's already begun
enjoy, be fond of or approve
per centum
an approximate attribute or quality of something
The act of having and controlling property.
a physical protuberance, large or small
estimate or calculate the numerical value of something
a portion or percentage of a whole
lightest most abundant element, atomic number 1
a unit cost of something
move back and forth
a cyclical span of time defined by a calendar system
face an event and survive; endure
(medicine) the capacity of an organism to defend against disease.
a statement, an idea expressed in language
perform a duty
soldier who is a member of a special forces group
become master of, overcome, dominate
area of damaged, cut tissue on a living body
revolve
twice the quantity or amount of something
Give an incentive for action, provoke or    stir up, urge on, or cause to act.
Funeral direction
to ignite or catch on fire
a large approximate quantity of some attribute or thing
Medicines
a method for solving a problem
reach an objective, attain a benchmark or goal
take something or someone away from somewhere
a stiffener in a garment
a constructed underground passageway
discern, identify, or know because of a previous experience
move or change one's own position or quality
incorporate or enclose one thing in another
guide, draw through
rescue, redeem, deliver from harm or danger
beat (as if) with a whip or rod
the medical procedure of manually turning the uterus
create an edge or border
restart or continue an activity after an interruption or pause
capture, ensnare, discover in the act
initiation, founding, introduction of something
a creased or bent form
train, tutor or polish social behavior, taste, judgement
a mental activity of construing or interpreting
emphasize; draw attention to; underline
possibility for the future
require or need something that is missing
an indication or manifestation of something, often physical
a sudden, brief burst of light
come or bring back to life, health, existence or use
A mental state of sadness.
position or location change
(cause to) move through a space, circuit or system, usu. returning to the starting point
bring in to consonance
submit, send to
state of being unified
(Cause to) become different or aquire a different character, possibly during development.
an objective or goal
seize and take captive; take or gain possession of something
give, dedicate, pledge
decide a specific time for an event
value a thing or course of action more highly than another
give something for a cause
furnish with a guideline
a social entreaty, as courtship or pleading
fail in strength or health and cease to funtion
reply verbally or in written form to a question, comment, letter, or speech
a slight cut in a physical surface
cause something to be the case
a state of confinement in a small space
state or explain the meaning
cause animals to move
be against, resist
a quantity of money added to a bank account
cause to come into existence; create
Performance, routine
people assembled together in a rhythmic movement
a computer server
forbid, prohibit
computer text files
a speech act of acknowledging gratitude for something
a state of pretending to be someone, something else
Become widespread as an idea or feeling.
connect with; reach a target or goal
income collected as tax by a government
be the deciding factor in a state of affairs or an outcome
end
a region allocated to hold something
acknowledge a contribution or cause
arrangement
system of arrangement
close within bounds; deprive of freedom
to collect, acquire or gather
deliver a formal talk or reprimand at length
a pair of mated people, e.g., married
rot; become unfit for consumption
lock with one another
take vows, join or allow to join a religious order
activity of selling
give out, allow to have
secondary public school, grades 9-12
Benjamin Rush, physician and American Revolutionary leader.
luggage compartment of a car
have a monopoly of; control fully
reduce to a simpler molecular compound
cause to be interested
To remain emotionally or intellectually attached without necessary physical contact.
The act of transporting goods commerically at rates cheaper than express rates.
(An occurance of) the preservation or management of natural resources.
remove the pod, husk or shell
abstain from eating or from certain foods
feel hot or painful, as if burning
(cause to) return after being refused
a loss of function
administrator, one who controls resources
a stovetop, cooking appliance
rinse one's mouth with mouthwash, gargle
a quantity that is an approximate calculation
turn on an axis; spin
marriage offer
Try to gain favor by flattering or showing    submission or fear.
the public submission of a legal record
Shrivel, wither, or mature imperfectly, as of a flower.
have faith in, bet on
burst into flames (telic)
The cognitive processes and capacity to remember.
mechanism that controls the water level in a section of canal
an onerous worry or concern
someone or something that is the agent of fulfilling desired expectations
a sharp side of an object
the forceful hit of a ball
Fine powdery material; the remains of something destroyed or broken up.
throw with force
Treat a substance with some agent; add an    agent to.
pass through the esophagus
move freely (usually within a bounded space)
secrete or form water
the state of being in contact
a variant of a musical theme which employs notes of reduced duration from the original theme
time to come
the legal state of owing money
a style and size of typeset
demand, ask for, or take as one's due
Cause to be distrusted or rejected; damage the reputation of.
Give equal rights to, as of the right to vote.
(cause to) lengthen or expand in time or some abstract realm
a knapsack, bundle on the back
a person who delivers information or goods
having an intuitive understanding or appreciation of something
adherence to moral principles
have sex with
partake or provide a banquet
idolize/show devotion to
make a ticking sound
run or flow slowly
Write a computer programme.
a state of social order in conformance with existing law
An electronic storage device.
a reference line used to align lettering
the quality of dedication, fixity of purpose
Look furtively at something.
cast off, get rid of something
recover posession or get back again
a coefficient assigned to elements in a frequency distribution in order to indicate the relative importance of each element
(cause to) move around
become due
Provide medical care for.
do over, make new
coat, cover, or treat the surface of
BASEBALL: strike out a batter
Regain or make up for, as of a financial loss.
duplicate, match
indistinct vocal articulations
the act of shoving something away
move or drive forcefully as if by a punch.
an intense surprise, often unpleasant
Make less emotionally hostile; win over mentally or emotionally.
informal usage for a domestic cat, often young
The act of sorting one thing from others.
the act of physically affixing or connecting things
the people who dwell in a camp
An unforseen development.
bring into general popular/common use
make impure; make radioactive
a long, thin implement, usually made of wood
Formally reject or disavow a formerly held    belief; retract a statement.
(Cause to) to turn to the left side.
Entail as a necessary accompaniment or result
Physically attach to something.
assign to a particular task
culminating event
act of constructing
the primary information-processing component of a computer, of a microprocessor chip
consider again, usually with a view to changing
cancel, reverse, annul
define; determine essential qualities of
The pursuit and killing or capture of animals.
manipulate the registers of a musical instrument
move in an irregular course
the legal granting or bestowment of rights
jolt of electrical current to the body
Become pregnant.
the bestowing of a franchise, enfranchisment
act of distributing
The state of being more than satisfied or satiated.
conscious subjective emotion or desire
The act of proposing someone as a candidate for something.
to hunt snipe
hunt for hawks
a continuous horizontal layer of brick
a brief attempt at some endeavor
propagation of waves back from a surface
examine minutely; search diligently
enjoy, be fond of or approve
a person under the guardianship or custody of another
Strike or hit sharply, perhaps as in sport.
Fly in or on a glider plane
one fourth of a year
make an abstract thing tight or more restrictive
provide (a person or group) with power or authority
Run at a moderately swift pace, as for exercise.
Envelop completely; form a cover over.
mold for casting, setting concrete
cause friction; wear away or erode
A feeling for something that is desired intensely.
put or store in a bottle
Be a sign or indication of; have as a meaning.
the act of choosing, picking one among many
act of becoming distant, moving away from
take in marriage; perform a marriage ceremony
Skip, leap, or move up and down or sideways as a manner of motion.
a military fortification, stronghold
the fashionable elite of a given community
characteristic call of an animal
make or become more acute or intense
A journey for some purpose.
Fake or falsify; control in a fraudulent manner.
fruit cooked down with sugar, jam
the act of leading or guiding others
coating used as a repellant to moisture
a person who cleans chimneys
Shine intensely; emit a steady, even light    from a source.
behave in a certain way; have a specific effect or outcome
communicate with a hiss
a male bee whose function is to fertilize the queen
a sporting match, boxing
cause to reproduce (plants or animals)
The feeling of annoyance at being hindered or thwarted.
move along on wheels
[provide with a new or different resting place
activity of extracting ore or other matter from the ground
to interrupt, enter uninvited
A person or thing equal to another in value or significane.
a cause or source of harm
make (more) comprehensible
the state of being retained
a problem, something wrong
stretch out or over completely
continue to live, exist, or prosper
a caress with the lips
Provide with rails, as by laying.
the close approach of one object to another
make a hole with a pointed tool
Fix (something old) with a new part; improve.
the act of  damaging something, causing breakage
the ground level of a multi-storied building
A quantity of something
a large room
leave out, omit, prevent from entering
Sniff at inquiringly.
provide lodging for
Change the purpose or functioning of a system.
provide with tools or items for a specific purpose
gain points in a game; add up (numbers)
travel in front of, be first, precede
Remove by rubbing
the hard upper part of the human nose
make shine
recover, return to former state
remove the pod, husk or shell
speed up the process of
(cause to) swing and miss the third pitch, striking out
Literally and quantifiably raise in social rank or status.
alter by chemical means, to make images.
highly infectious viral disease
make a certain size or sort according to size
Remove tree stumps from an area.
pass the tongue over, take up with the tongue
a computer-generated visual image
do better in a competition; win
aerodynamic force that opposed gravity
Start abruptly, ignite
a local announcement made within a public broadcast
making real or concrete
Sound sharply or shrilly, as if stabbing.
the act of officially gaining entrance to somewhere
spend time in prison
hold, contain within
act of verbally informing
forcefully request
the quality of dissimilarity
throw quickly, flick
move to and fro around a particular point or points, remain near
Put up with something or someone unpleasant; show deference towards someone or some authority.
the breakup or termination of a social relationship
destruction of a ship
The human act of making something new.
ride (a bicycle or motorcycle)
The principal activity that one does to earn money.
maintain or preserve a particular condition, state, or activity
a spoken or written reply to a question
committee with supervisory powers
a section of a composition, textual or musical
a gear for changing the motion of a machine to the opposite direction
take a digestion-aiding walk after a meal
forked form or shape
insert or close with a plug
incite some act of insubordination
formally announce the termination of an agreement
beat or pound rapidly
Make a line or marks on a surface; copy by following the lines of
recite as a chant, intone
the ability of computers to exchange digital information between them and make use of it
act of applying force
the process of becoming less or smaller
Be in command of; exercise authority or  control over.
be a factor or play a role in something
an expanse of land
move an implement through with a circular motion, mix
deprive of courage, hope, or optimism
the armed forces
Furnish with metal spurs.
Remove water from a substance.
Provide with rails, as by laying.
make or be a summary
be affected with smut or mildew
Talk or behave amorously; have sex with.
Excuse some minor social infraction; accept    an excuse for something.
indicate, be signs or symptoms of
a verbal or written request for access to something.
have a logical result
Travel on foot.
the act of suction
A band of leather or rope made to identify animals.
Put a harness on.
Provide or equip specifically with furniture.
To move (the head or body) quickly    downwards or away (from something).
reason or establish by deduction
Make pregnant; fertilize and cause to grow.
find the solution to or understand the          meaning of
Move about in a confused or purposeless manner.
put up with something unpleasant
A means or agency by which something is communicated.
take apart into constituent pieces
A short theatrical program that is part of a longer program.
decree, issue an order
the termination, ending of a meeting
An indicator that orients one generally.
Increase the acoustic volume of.
open to members of all races and ethnic groups
reduce to small pieces by pounding or abrading
hit a fly (baseball)
FISHING-exhaust by allowing to pull on the line
be or come into conflict
Produce a phonemic click.
angle with a hook and line and draw through water
the front face of a building
make something more diverse or varied
issue bonds on something
women's sleeveless undergarment
Move oneself very fast.
Oppose or approach, as in hostility or competition
make more complex or intricate
cause to be/show to be invalid
create a new entity by putting components or members together
the activity of providing supplies
make or become brighter or lighter
actors in a play
take someone's soul into heaven
Discipline in personal or social activities.
a person eating a meal, often in a restaurant
A sudden (emotional) outburst.
make or become harder
the introductory portion of a story
work material with a tool
make or become less strict or severe
support by placing on or against something solid
Part, cease, or break association with (something).
have the financial means to obtain, buy, or do something
A list of candidates nominated by a political party to run for election.
say, state, or perform again
allow the passage of air
unite; merge with something already in existence
use resources or materials
make or become harder
(Cause to) be firmly attached or closed.
make or become wider or more extensive
Unrestricted freedom to use something.
laying claim, the act of taking possession of, or power over, something.
give structure to
mark with scars
(Baseball) Tag the base runner to get him out.
Be reflected as heat, light, or shock waves;.
a competitor most likely to win
make or become longer
be on base at the end of an inning
Either of the two categories (male or female) into which most organisms are divided, or key properties that distinguish the categories.
(science) The atomic weight of an element that has the same combining capacity as a given weight of another element.
provide with a toggle
(cause to) move back and forth or side to side
measure the depth of a body of water
make a loud noise, as an animal
The act of coming to rest after a voyage.
join or bring together (two) objects, ideas, or people
Make reference to something.
Behave violently, as if in a state of great    anger; break or tear violently.
Form a spiral or move in a spiral course.
a feeling of aversion, distaste, antipathy
Release (gas or energy) as a result of a chemical reaction or physical decomposition (perhaps in a metaphorical sense)
overcome with amazement; flabbergast
behave in a condescending manner
take into one's family or group
(Contract bridge) the highest bid becomes the contract setting in the number of tricks that the bidder must make.
go on a campaign or off to war
react, or respond to a call, command, or stimulus
(Literally) sound with resonance.
an injury to the skin caused by radiation, heat or chemicals
make a causal or logical connection; group together
Apply conditioner to make smooth and shiny.
exercise or have power of memory
A feeling or state of extreme anger.
Wrinkled, crumpled, or creased.
Admit to testing/proof
an accounting period of 12 months
praise the qualities in order to sell or promote
relative darkness of a color
Strike heavily, especially with a fist or implement.
Make receptive or willing toward an action or belief.
open again, open anew
A conceptual formulation; a way of conceiving something.
heat metal prior to working it
feel the strong need to eat
be in direct physical contact with
Give suck to; receive suck.
covering for a bed
grip, penetrate or tear off with or as if with the teeth or jaws
play a stringed instrument with a bow
Doubt or an impression that something might    not be the case.
A successful throw or try for a point after a touchdown.
Express audibly; utter sounds (not          necessarily words); indicate.
hold back in uncertainty or unwillingness
a serving of a beverage
Give an incentive or reason for action.
the day before today
take or capture by force or authority
(Medicine) Apply a plaster cast to.
interfere with someone else's activity
Assign a new time for an event
Value measured by what must be given or done to obtain something.
touch or hold with the hands
scrape or rub to relieve itching
give a detailed account of
Any activity that acquires a person's attention.
cause to become awake or alert
provide a feast or banquet
The act of hindering someone's plans or efforts.
Fool or hoax (someone).
raising the stakes or bet of a game by a factor of 2
The (period of) control of a country by military forces of a foreign power.
(computer science) the occurence of an incorrect result produced by a computer.
give pleasure, satifaction, or happiness; be pleasing to
the soft padding under a saddle
elecate or idealize in allusion to Christ's          transfiguration
inability of the heart to pump blood to sustain life functions
cause to vibrate in a different pattern
warn or call to a sense of preparedness
Adapt oneself to new or different conditions.
move faster, or cause to move faster
Supply food ready to eat, as for parties          and banquets.
express protest or dissent, raise an objection
clear mucus from one's throat
pass into a state or condition
add embellishments (to medieval manuscripts)
make or become thick
move or execute very quickly or hastily
hurt, feel physical pain
tell or spread rumors; say
throw with force or recklessness; cast away
return to in thought or speech
Try to cure by special care    or treatment, of an illness or injury; treat carefully.
a sweeping cut with a sharp instrument
Any address at which one dwells more than temporarily.
BOTANY -- the usually underground organ of a plant that lacks buds or leaves or nodes, which absorbs water and mineral salts; usually it anchors the plant to the ground.
Reproduce or make an exact copy of; make,    do, or perform again.
pass (a thread) through or into,on or as if on a    string
Engrave by means of dots.
have a strong desire to do something
be relevant or pertinent
indicate contempt by breathing noisily and    forcefully through the news
become aware of through the senses
cause to be alert or energetic
Remove by erasing or crossing out, as if by    drawing a line through.
cook slowly for a long time in liquid
the temperature at which a liquid boils, bubbles
Make a map of; show or establish the    features or details of; explore or survey for the purposes of making a    map.
The social force that binds one to the courses of action demanded by that force.
Avoid or try to avoid fulfilling,          answering, or performing.
Lead someone in the wrong direction  (physically); give wrong spatial directions.
maintain, assert, or claim
Improve by restoring to a previous or better condition.
make accessible to some action, influence, or condition
Approach a limit as the number of terms          increases without limit; specifically a mathematical term.
The stiching that forms the rim of a shoe or boot.
put under the control of the federal government
Manual stimulation fo the genital area for sexual pleasure.
equip with a fuse
end of the American Revolution
(mathematics) The smallest whole number or a numeral representing this number.
stimulating or demanding factors of a situation
A physical portion of all or part of a natural object that is collected and preserved as an example of its class or total.
Amplify an electron current by causing part          of the power on the output circuit to act upon the input circuit.
A human utters words loudly and forcefully.
oppose with equal weight or force
(Cause to) strike the air in flight.
Preserve in a tin or jar.
a chain of atoms in a molecule forming a closed loop
make the surface level or smooth
Impress or affect deeply (emotionally).
Supply with a constant flow of some liquid,          for the purpose of cooling, cleansing, or disinfecting.
apply a thin coating of paint/metal
disregard, fail to attend to
make a short, sharp sound
connect or fasten into a chain by linking
beat thoroughly in a competition or fight
Look through a book or some other written    material (for information).
state of being active
(theology) the act of deliving from sin or saving from evil.
declare or make unfit or unsuitable
Cook on a hot surface using fat; cook in oil.
Improve by pruning or polishing
An area that is central within some larger region.
start suddenly, as if from fright
Be larger in number, quantity, power,          status, or importance, without personally having sovereign power.
Give oneself entirely to a specific person,    activity, or cause.
the finger next to the thumb, forefinger
put up with something or someone (unpleasant)
role or function of the head of a government department
consider again, usually with a view to changing
drive at an excessive or illegal velocity
an air force military unit, smaller than a squadron
the act of giving someone a job, hiring someone
the act of annoying someone or something
Droop, sink, or settle from or as if from          pressure or loss of tautness; become less intense.
Cite as an authority; resort to (as in an argument).
Allocate or dedicate; set aside or apart          for a specific purpose or use.
make out of components (in an improvising manner)
rehearse, learn by repetition
Intend something to move toward a certain goal
register formally as a participant
write as if with print, not in cursive
shoot down birds
appoint to clerical posts
(Chemistry) Undergo sequestration by          forming a stable compound with an ion.
Furnish with an opening to allow air to          circulate or gase to escape.
Stand with arms or forelegs raised, as of menacing.
accounting period of 12 months
Convert the genetic information in a strand          of DNA into a strand of RNA.
give a thrashing to, beat
Immerse or be immersed in a boiling liquid,  for purposes of cooking or processing.
Supply with water for crops, as with    channels, ditches, or streams.
The period of time during which a leasing contract is in effect.
Protect or strengthen with sandbags.
travel; make a tour of a place
the political head of city government
pass the tongue over, take up with the tongue
A city on the River Thames in Bershire, southern England
a chart or map showing the dimensions, movements or progress of something
Take away a vital or essential part of          something.
An imperfection; the quality of being inadequate or falling short.
Pat or squeeze, especially under the chin.
make up something fictional or untrue
the act of swallowing liquid
have unlawful sex with a prostitute
Reverse the winding or twisting of;  separate the tangles of.
Reimburse or compensate (someone), as for a          loss or sacrifice.
sharpen a razor with a strap
the longer telegraphic signal of Morse Code
The part of the body between the neck and upper arm, or any of its subparts.
Rain, hail, or snow hard and be very windy,    often with thunder and lightning; blow hard.
fill with high spirits
The release of pressure built up during a stop consonant.
(cause to) fall in drops
The status of being champion.
Draw advantages from; take advantage.