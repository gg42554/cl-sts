restate (words) from one language into another language.
a group of nations having common interests.
soften or disintegrate by means of chemical action, heat, or moisture.
devote oneself to a special area of work.
a porch that resembles the deck on a ship.
either of the two halves of a bow from handle to tip.
a supplementary part or accessory.
place limits on (extent or access).
be opposite.
cause to become alive again.
something with no concrete substance.
a name given to a product or service.
a set sequence of steps, part of larger computer program.
cover the front or surface of.
estimate the value of.
a mental representation of the meaning or significance of something.
an occasion on which people can assemble for social interaction and entertainment.
make fine adjustments or divide into marked intervals for optimal measuring.
a narrative song with a recurrent refrain.
handle effectively.
appoint someone to (a position or a job).
organization of performers and associated personnel (especially theatrical).
office furniture consisting of a container for keeping papers in order.
signal by winking.
any of the equal portions into which the capital stock of a corporation is divided and ownership of which is evidenced by a stock certificate.
establish or strengthen as with new evidence or facts.
a communication (usually brief) that is written or spoken or signaled.
a denture anchored to teeth on either side of missing teeth.
terminate an association with.
reduce or cause to be reduced from a solid to a liquid state, usually by heating.
treat the body or any part of it by wrapping it, as with blankets or sheets, and applying compresses to it, or stuffing it to provide cover, containment, or therapy, or to absorb blood.
make underscoring marks.
surround completely.
the end of a word (a suffix or inflectional ending or final morpheme).
an assertion that someone is guilty of a fault or offence.
create an image or likeness of.
bend into the shape of a crank.
mock or make fun of playfully.
be extremely bad in quality or in one's performance.
the relative position or standing of things or especially persons in a society.
consider in detail and subject to an analysis in order to discover essential features or meaning.
persuade somebody to accept something.
matter that remains after something has been removed.
anything that approximates the shape of a column or tower.
become inflated.
a heavy toxic silvery-white radioactive metallic element; occurs in many isotopes; used for nuclear fuels and nuclear weapons.
say mean things.
be offended or bothered by; take offense with, be bothered by.
a number that expresses the accomplishment of a team or an individual in a game or contest.
subject to political, religious, or moral censorship.
a casual or unexpected convergence.
a connection between an electrical device and a large conducting body, such as the earth (which is taken to be at zero voltage).
the act of ending something.
a well-known or notable person.
an agent that operates some apparatus or machine.
determine or indicate the place, site, or limits of, as if by an instrument or by a survey.
act in unison or agreement and in secret towards a deceitful or illegal purpose.
be tantamount or equivalent to.
cause to move by pulling.
a voluntary gift (as of money or service or ideas) made to some worthwhile cause.
a loud utterance of emotion (especially when inarticulate).
go back to a previous state.
a body of people who settle far from home but maintain ties with their homeland; inhabitants remain nationals of their home state but are not literally under the home state's system of government.
a formal series of statements showing that if one thing is true something else necessarily follows from it.
cause to move; cause to be in a certain position or condition.
earnest and conscientious activity intended to do or accomplish something.
a system of units used to express the weight of something.
some situation or event that is thought about.
feel or have a desire for; want strongly.
a blank character used to separate successive words in writing or printing.
a facility where things can be deposited for storage or safekeeping.
make a certain noise or sound.
cause to be in brief contact with.
get to know or become aware of, usually accidentally.
the chief executive of a republic.
touch with the lips or press the lips (against someone's mouth or other body part) as an expression of love, greeting, etc..
the force used in pulling.
a gradual decrease; as of stored charge or current.
a number or letter indicating quality (especially of a student's performance).
an impetuous rush toward someone or something.
suffer failure, as in some enterprise.
the property of copious abundance.
a short note recognizing a source of information or of a quoted passage.
(often followed by `of') a large number or amount or extent.
sway to and fro.
a change for the better.
a sacrament admitting a baptized person to full participation in the church.
a courteous expression (by word or deed) of esteem or regard.
cause (a computer) to execute a single command.
give to, and receive from, one another.
a punctuation mark (.) placed at the end of a declarative sentence to indicate a full stop or after abbreviations.
something craved, especially an intravenous injection of a narcotic drug.
increase rapidly and in an uncontrolled manner.
make high-pitched sounds.
a special variety of domesticated animals within a species.
the discipline that records and interprets past events involving human beings.
a person who is the aim of an attack (especially a victim of ridicule or exploitation) by some hostile person or influence.
military personnel lost by death or capture.
use cunning or deceit to escape or avoid.
a loss entailed by giving up or selling something at less than its value.
a sudden sharp decrease in some quantity.
change the key of, in music.
a message refusing to accept something that is offered.
move so that an opening or passage is obstructed; make shut.
cut the surface of; wear away the surface of.
a fruiting structure resembling an umbrella or a cone that forms the top of a stalked fleshy fungus such as a mushroom.
a list of particulars (as a playbill or bill of fare).
a diacritical mark used to indicate stress or placed above a vowel to indicate a special pronunciation.
the trait of believing in the honesty and reliability of others.
an attempt to get something.
go up or advance.
clean with hard rubbing.
become injured, broken, or distorted by pressure.
point or cause to go (blows, weapons, or objects such as photographic equipment) towards.
give or convey physically.
do lacework.
an open jar of glass or porcelain used as an ornament or to hold flowers.
preserve with sugar.
a motorboat with an open deck or a half deck.
a process of becoming larger or longer or more numerous or more important.
be careful or certain to do something; make certain of something.
give something useful or necessary to.
edge tool used in shaving.
learn by reading books.
lengthen or extend in duration or space.
refrain from doing something.
something that provides direction or advice as to a decision or course of action.
a representation of a person that is exaggerated for comic effect.
a separate and self-contained entity.
make less severe or harsh.
a judgment of the qualities of something or somebody.
beat severely with a whip or rod.
shape or influence; give direction to.
produce buds, branches, or germinate.
a particular environment or walk of life.
a vertical array of numbers or other information.
be cognizant or aware of a fact or a specific piece of information; possess knowledge or information about.
any likeness of a person, in any medium.
any of various controls or devices for regulating or controlling fluid flow, pressure, temperature, etc..
cause to form a united, orderly, and aesthetically consistent whole.
informal terms for the leg.
the goal intended to be attained (and which is believed to be attainable).
place or set apart.
assets belonging to or due to or contributed by an individual person or group.
an event that accomplishes its intended purpose.
place in a line or arrange so as to be parallel or straight.
bedclothes consisting of a lightweight cloth covering (an afghan or bedspread) that is casually thrown over something.
a protective covering consisting of netting; can be mounted in a frame.
a measure of how likely it is that some event will occur; a number expressing the ratio of favorable cases to the whole number of cases possible.
someone who frequently finds fault or makes harsh and unfair judgments.
recovery or preservation from loss or danger.
observe with care or pay close attention to.
to compel or deter by or as if by threats.
extend out or project in space.
be the host of or for.
make invalid for use.
feel about or towards; consider, evaluate, or regard.
a proportion in relation to a whole (which is usually the amount per hundred).
an approximate definition or example.
the state or fact of being an owner.
a natural elevation (especially a rocky one that juts out into the sea).
evaluate or estimate the nature, quality, ability, extent, or significance of.
the part played by a person in bringing about a result.
a nonmetallic univalent element that is normally a colorless and odorless highly flammable diatomic gas; the simplest and lightest and most abundant element in the universe.
a single chance or instance.
move or sway in a rising and falling or wavelike pattern.
a period of time containing 365 (or 366) days.
face and withstand with courage.
(medicine) the condition in which an organism can resist disease.
a brief statement.
do duty or hold offices; serve in a specific function.
a member of a military unit trained as shock troops for hit-and-run raids.
get on top of; deal with successfully.
a casualty to military personnel resulting from combat.
change directions as if revolving on a pivot.
a quantity that is twice as great as another.
provoke or stir up.
the trade of a funeral director.
catch fire.
a concentrated example of something.
a substance that is used as a medicine or narcotic.
the successful action of solving a problem.
develop in a positive way.
go away or leave.
(nautical) brace consisting of a heavy rope or wire cable used as a support for a mast or spar.
a passageway through or under something, usually underground (especially one for trains or cars).
be fully aware or cognizant of.
move very slightly.
incorporate a food ingredient into a mixture by repeatedly turning it over without stirring or beating.
pass over, across, or through.
refrain from harming.
strike as if by whipping.
manual turning of a fetus in the uterus (usually to aid delivery).
enclose in or as if in a frame.
take up or begin anew.
discover or come upon accidentally, suddenly, or unexpectedly; catch somebody doing something or in a certain state.
the act of starting something for the first time; introducing something new.
an angular or rounded shape made by folding.
teach or refine to be discriminative in taste or judgment.
an interpretation of a text or action.
draw a line or lines underneath to call attention to.
a prediction of the course of a disease.
have need of.
an indication of damage.
a momentary brightness.
cause to regain consciousness.
a mental state characterized by a pessimistic sense of inadequacy and a despondent lack of activity.
the act of changing the location of something.
move in circles.
bring (several things) into consonance or relate harmoniously.
cause to move; cause to be in a certain position or condition.
the state of being joined or united or linked.
become distinct and acquire a different character.
the goal intended to be attained (and which is believed to be attainable).
take possession of by force, as after an invasion.
give entirely to a specific person, activity, or cause.
make a schedule; plan the time and place for events.
like better; value more highly.
give to a charity or good cause.
furnish with rubrics or regulate by rubrics.
a petition or appeal made to a person of superior status or rank.
break down, literally or metaphorically.
react verbally.
a slight surface cut (especially a notch that is made to keep a tally).
cause to move; cause to be in a certain position or condition.
the act of forcing yourself (or being forced) into or through a restricted space.
give a definition for the meaning of a word.
pursue for food or sport (as of wild animals).
act against or in opposition to.
money deposited in a bank or some similar institution.
bring into being.
a short theatrical performance that is part of a longer program.
a party of people assembled for dancing.
(computer science) a computer that provides client stations with access to files and printers as shared resources to a computer network.
forbid the public distribution of ( a movie or a newspaper).
(computer science) a computer file that contains text (and possibly formatting instructions) using seven-bit ASCII characters.
an acknowledgment of appreciation.
an outward semblance that misrepresents the true nature of something.
cause to become widely known.
perceive with the senses quickly, suddenly, or momentarily.
government income due to taxation.
shape or influence; give direction to.
put an end to.
the particular portion of space occupied by something.
have trust in; trust in the truth or veracity of.
logical or comprehensible arrangement of separate elements.
an arrangement scheme.
to close within bounds, limit or hold back from movement.
get or gather together.
censure severely or angrily.
a pair of people who live together.
become unfit for consumption or use.
become engaged or intermeshed with one another.
take vows, as in religious order.
the general activity of selling.
allow to have.
a public secondary school usually including grades 9 through 12.
physician and American Revolutionary leader; signer of the Declaration of Independence (1745-1813).
British term for the luggage compartment in a car.
have or exploit a monopoly of.
reduce (petroleum) to a simpler compound by cracking.
attract; cause to be enamored.
to remain emotionally or intellectually attached.
transporting goods commercially at rates cheaper than express rates.
the preservation and careful management of the environment and of natural resources.
fall out of the pod or husk.
abstain from eating.
feel hot or painful.
come back after being refused.
loss of ability to function normally.
someone who controls resources and expenditures.
a kitchen appliance used for cooking food.
rinse one's mouth and throat with mouthwash.
an approximate calculation of quantity or degree or worth.
turn on or around an axis or a center.
an offer of marriage.
show submission or fear.
the entering of a legal document into the public record.
shrivel or wither or mature imperfectly.
have faith or confidence in.
start to burn or burst into flames.
the cognitive processes whereby past experience is remembered.
enclosure consisting of a section of canal that can be closed to control the water level; used to raise or lower vessels that pass through it.
an onerous or difficult concern.
someone (or something) on which expectations are centered.
a sharp side formed by the intersection of two surfaces of an object.
hitting a golf ball off of a tee with a driver.
the remains of something that has been destroyed or broken up.
utter with force; utter vehemently.
treat with an agent; add (an agent) to.
pass through the esophagus as part of eating or drinking.
cause to move or operate freely within a bounded space.
secrete or form water, as tears or saliva.
the state of being connected.
the statement of a theme in notes of lesser duration (usually half the length of the original).
the time yet to come.
the state of owing something (especially money).
a specific size and style of type within a type family.
demand as being one's due or property; assert one's right or title to.
damage the reputation of.
give equal rights to; of women and minorities.
lengthen in time; cause to be or last longer.
a bundle (especially one carried on the back).
a person who conveys (carries or transmits).
an intuitive understanding of something.
adhering to moral principles.
have sexual intercourse with.
partake in a feast or banquet.
show devotion to (a deity).
make a clicking or ticking sound.
run or flow slowly, as in drops or in an unsteady stream.
write a computer program.
a state of order in which events conform to the law.
an electronic memory device.
a light line that is used in lettering to help align the letters.
the trait of sincere and steadfast fixity of purpose.
look furtively.
get rid of.
get or find back; recover the use of.
(statistics) a coefficient assigned to elements of a frequency distribution in order to represent their relative importance.
cause to move round and round.
become due for repayment.
provide veterinary care for.
make new.
put a coat on; cover the surface of; furnish with a surface.
strike out (a batter), (of a pitcher).
regain or make up for.
duplicate or match.
indistinct articulation.
the act of applying force in order to move something away.
drive forcibly as if by a punch.
an unpleasant or disappointing surprise.
make less hostile; win over.
informal terms referring to a domestic cat.
sorting one thing from others.
the act of fastening things together.
a group of people living together in a camp.
an unforeseen development.
cater to popular taste to make popular and present to the general public; bring into general or common use.
make impure.
an implement consisting of a length of wood.
formally reject or disavow a formerly held belief, usually under pressure.
turn or go to the port or left side, of a ship.
impose, involve, or imply as a necessary accompaniment or result.
fix to; attach.
assign to a specific task.
be the culminating event.
the act of constructing something.
(computer science) the part of a computer (a microprocessor chip) that does most of the data processing.
consider again (a bill) that had been voted upon before, with a view to altering it.
cancel, annul, or reverse an action or its effect.
determine the essential quality of.
the pursuit and killing or capture of wild animals regarded as a sport.
manipulate the registers of an organ.
move to and fro or from place to place usually in an irregular course.
the act of granting rights.
a reflex response to the passage of electric current through the body.
become pregnant; undergo conception.
the act of certifying or bestowing a franchise on.
the act of apportioning or distributing something.
the state of being more than full.
the conscious subjective aspect of feeling or emotion.
the condition of having been proposed as a suitable candidate for appointment or election.
hunt or shoot snipe.
hunt with hawks.
(construction) a layer of masonry.
a usually brief attempt.
the phenomenon of a propagating wave (light or sound) being thrown back from a surface.
examine minutely.
be fond of.
a person who is under the protection or in the custody of another.
strike sharply, as in some sports.
fly in or as if in a glider plane.
a fourth part of a year; three months.
make tight or tighter.
provide with power and authority.
run at a moderately swift pace.
form an impenetrable cover over.
a mold for setting concrete.
wear away or erode.
something that is desired intensely.
store (liquids or gases) in bottles.
be a sign or indication of.
the act of choosing or selecting.
the act of becoming more distant.
take in marriage.
skip, leap, or move up and down or sideways.
a stronghold.
the fashionable elite.
the characteristic utterance of an animal.
make more intense, stronger, or more marked.
a journey for some purpose (usually including the return).
manipulate in a fraudulent manner.
fruit preserved by cooking with sugar.
the activity of leading.
a finishing coat applied to exclude moisture.
someone who cleans soot from chimneys.
emit a steady even light without flames.
behave in a certain way.
express or utter with a hiss.
stingless male bee in a colony of social bees (especially honeybees) whose sole function is to mate with the queen.
a boxing or wrestling match.
cause to procreate (animals).
a feeling of annoyance at being hindered or criticized.
move along on or as if on wheels or a wheeled vehicle.
provide with a new seat.
the act of extracting ores or coal etc from the earth.
enter uninvited.
a person or thing equal to another in value or measure or force or effect or significance etc.
a cause of pain or injury or loss.
make clear and (more) comprehensible.
the act of retaining something.
a problem.
stretch out completely.
continue to live through hardship or adversity.
the act of caressing with the lips (or an instance thereof).
lay with rails.
the motion of one object relative to another.
make a hole, especially with a pointed power or hand tool.
piece (something old) with a new part.
the act of breaking something.
the floor of a building that is at or nearest to the level of the ground around the building.
a quantity of money.
the large room of a manor or castle.
prevent from entering; keep out.
sniff or smell inquiringly.
provide housing for.
change the inherent purpose or function of something.
provide with (something) usually for a specific purpose.
gain points in a game.
travel in front of; go in advance of others.
remove by or as if by rubbing or erasing.
the hard ridge that forms the upper part of the nose.
make (a surface) shine.
return to a former condition.
remove the husks from.
speed up the progress of; facilitate.
strike out by swinging and missing the pitch charged as the third.
raise in rank, character, or status.
make visible by means of chemical solutions.
an acute febrile highly contagious viral disease.
make to a size; bring to a suitable size.
remove tree stumps from.
pass the tongue over.
an image that is generated by a computer.
come out better in a competition, race, or conflict.
the component of the aerodynamic forces acting on an airfoil that opposes gravity.
start abruptly.
(broadcasting) a local announcement inserted into a network program.
something that is made real or concrete.
sound sharply or shrilly.
the act of admitting someone to enter.
spend time in prison or in a labor camp.
contain or hold; have within.
the act of informing by verbal report.
request urgently and forcefully.
the quality of being unlike or dissimilar.
throw quickly.
move to and fro.
put up with something or somebody unpleasant.
the termination or disintegration of a relationship (between persons or nations).
destroy a ship.
the act of starting something for the first time; introducing something new.
ride a motorcycle.
the principal activity in your life that you do to earn money.
continue a certain state, condition, or activity.
a statement (either spoken or written) that is made to reply to a question or request or criticism or accusation.
a committee having supervisory powers.
a short section of a musical composition.
the gears by which the motion of a machine can be reversed.
take a walk for one's health or to aid digestion, as after a meal.
a part of a forked or branching shape.
fill or close tightly with or as if with a plug.
incite, move, or persuade to some act of lawlessness or insubordination.
announce the termination of, as of treaties.
cause to throb or beat rapidly.
make a mark or lines on a surface.
recite with musical intonation; recite as a chant or a psalm.
(computer science) the ability to exchange and use information (usually in a large heterogeneous network made up of several local area networks).
the act of applying force suddenly.
a process of becoming smaller or shorter.
exercise authoritative control or power over.
be or play a part of or in.
an extended area of land.
move an implement through.
deprive of courage or hope; take away hope from; cause to feel discouraged.
a force that is a branch of the armed forces.
furnish with spars.
remove water from.
provide with rails.
be a summary of.
affect with smut or mildew, as of a crop such as corn.
talk or behave amorously, without serious intentions.
accept an excuse for.
be a signal for or a symptom of.
a verbal or written request for assistance or employment or admission to a school.
have as a logical consequence.
cross on foot.
the act of sucking.
a band of leather or rope that is placed around an animal's neck as a harness or to identify it.
put a harness.
provide or equip with furniture.
to move (the head or body) quickly downwards or away.
reason by deduction; establish by deduction.
fertilize and cause to grow.
find the solution to (a problem or question) or understand the meaning of.
move about in a confused manner.
put up with something or somebody unpleasant.
a means or agency by which something is expressed or communicated.
take apart into its constituent pieces.
a short theatrical performance that is part of a longer program.
issue an order.
the termination of a meeting.
an indicator that orients you generally.
increase the volume of.
open (a place) to members of all races and ethnic groups.
reduce to small pieces or particles by pounding or abrading.
hit a fly.
exhaust by allowing to pull on the line.
be incompatible; be or come into conflict.
produce a click.
angle with a hook and line drawn through the water.
the face or front of a building.
make something more diverse and varied.
issue bonds on.
a woman's sleeveless undergarment.
move very fast.
oppose, as in hostility or a competition.
make more complex, intricate, or richer.
show to be invalid.
create by putting components or members together.
the activity of supplying or providing something.
make lighter or brighter.
the actors in a play.
take up someone's soul into heaven.
discipline in personal and social activities.
a person eating a meal (especially in a restaurant).
a sudden outburst.
make hard or harder.
the introductory section of a story.
work with a tool.
make less severe or strict.
support by placing against something solid or rigid.
part; cease or break association with.
have the financial means to do something or buy something.
a list of candidates nominated by a political party to run for election to public offices.
to say, state, or perform again.
allow the passage of air through.
unite or merge with something already in existence.
use up (resources or materials).
become hard or harder.
cause to be firmly attached.
become broader or wider or more extensive.
unrestricted freedom to use.
the act of taking possession of or power over something.
give a structure to.
mark with a scar.
tag the base runner to get him out.
be reflected as heat, sound, or light or shock waves.
a competitor thought likely to win.
become long or longer.
to be on base at the end of an inning, of a player.
either of the two categories (male or female) into which most organisms are divided.
the atomic weight of an element that has the same combining capacity as a given weight of another element; the standard is 8 for oxygen.
provide with a toggle or toggles.
cause to move back and forth.
measure the depth of (a body of water) with a sounding line.
make a loud noise, as of animal.
the act of coming to land after a voyage.
bring two objects, ideas, or people together.
make reference to.
behave violently, as if in state of a great anger.
move in a spiral or zigzag course.
a feeling of aversion or antipathy.
release (gas or energy) as a result of a chemical reaction or physical decomposition.
overcome with amazement.
behave in a patronizing and condescending manner.
take into one's family.
(contract bridge) the highest bid becomes the contract setting the number of tricks that the bidder must make.
go on a campaign; go off to war.
react to a stimulus or command.
sound with resonance.
an injury caused by exposure to heat or chemicals or radiation.
make a logical or causal connection.
apply conditioner to in order to make smooth and shiny.
exercise, or have the power of, memory.
a state of extreme anger.
become wrinkled or crumpled or creased.
admit to testing or proof.
any accounting period of 12 months.
make a plug for; praise the qualities or in order to sell or promote.
relative darkness or lightness of a color.
strike heavily, especially with the fist or a bat.
make receptive or willing towards an action or attitude or belief.
open again or anew.
a way of conceiving something.
heat a metal prior to working it.
feel the need to eat.
be in direct physical contact with; make contact.
give suck to.
decorative cover for a bed.
to grip, cut off, or tear with or as if with the teeth or jaws.
play on a string instrument with a bow.
an impression that something might be the case.
a successful free throw or try for point after a touchdown.
express audibly; utter sounds (not necessarily words).
pause or hold back in uncertainty or unwillingness.
a single serving of a beverage.
give an incentive for action.
the day immediately before today.
take or capture by force.
apply a plaster cast to.
interfere in someone else's activity.
assign a new time and place for an event.
value measured by what must be given or done or undergone to obtain something.
touch, lift, or hold with the hands.
scrape or rub as if to relieve itching.
narrate or give a detailed account of.
any activity that occupies a person's attention.
cause to become awake or conscious.
provide a feast or banquet for.
an act of hindering someone's plans or efforts.
fool or hoax.
raising the stakes in a card game by a factor of 2.
the control of a country by military forces of a foreign power.
(computer science) the occurrence of an incorrect result produced by a computer.
give pleasure to or be pleasing to.
a soft pad placed under a saddle.
elevate or idealize, in allusion to Christ's transfiguration.
inability of the heart to pump enough blood to sustain normal bodily functions.
cause to vibrate in a definite pattern.
warn or arouse to a sense of danger or call to a state of preparedness.
adapt or conform oneself to new or different conditions.
cause to move faster.
supply food ready to eat; for parties and banquets.
express or raise an objection or protest or criticism or express dissent.
clear mucus or food from one's throat.
pass into a specified state or condition.
add embellishments and paintings to (medieval manuscripts).
make thick or thicker.
run or move very quickly or hastily.
feel physical pain.
tell or spread rumors.
throw with force or recklessness.
return in thought or speech to something.
try to cure by special care of treatment, of an illness or injury.
a strong sweeping cut made with a sharp instrument.
any address at which you dwell more than temporarily.
(botany) the usually underground organ that lacks buds or leaves or nodes; absorbs water and mineral salts; usually it anchors the plant to the ground.
reproduce or make an exact copy of.
thread on or as if on a string.
engrave by means of dots and flicks.
have a strong desire or urge to do something.
be pertinent or relevant or applicable.
indicate contempt by breathing noisily and forcefully through the nose.
to become aware of through the senses.
cause to be alert and energetic.
remove by erasing or crossing out or as if by drawing a line.
cook slowly and for a long time in liquid.
the temperature at which a liquid boils at sea level.
make a map of; show or establish the features of details of.
the social force that binds you to the courses of action demanded by that force.
avoid or try to avoid fulfilling, answering, or performing (duties, questions, or issues).
lead someone in the wrong direction or give someone wrong directions.
maintain or assert.
restore to a previous or better condition.
expose or make accessible to some action or influence.
approach a limit as the number of terms increases without limit.
the stitching that forms the rim of a shoe or boot.
put under the control and authority of a federal government.
manual stimulation of the genital area for sexual pleasure.
equip with a fuse; provide with a fuse.
the successful ending of the American Revolution.
the smallest whole number or a numeral representing this number.
a demanding or stimulating situation.
all or part of a natural object that is collected and preserved as an example of its class.
amplify (an electron current) by causing part of the power in the output circuit to act upon the input circuit.
utter words loudly and forcefully.
contrast with equal weight or force.
strike the air in flight.
preserve in a can or tin.
(chemistry) a chain of atoms in a molecule that forms a closed loop.
make the surface of level or smooth.
impress or affect deeply.
supply with a constant flow or sprinkling of some liquid, for the purpose of cooling, cleansing, or disinfecting.
apply a thin coating of paint, metal, etc., to.
fail to attend to.
make a sharp sound.
connect or arrange into a chain by linking.
beat thoroughly and conclusively in a competition or fight.
look through a book or other written material.
the state of being active.
(theology) the act of delivering from sin or saving from evil.
make unfit or unsuitable.
cook on a hot surface using fat.
improve or perfect by pruning or polishing.
an area that is approximately central within some larger region.
start suddenly, as from fright.
be larger in number, quantity, power, status or importance.
give entirely to a specific person, activity, or cause.
the finger next to the thumb.
put up with something or somebody unpleasant.
the role of the head of a government department.
consider again; give new consideration to; usually with a view to changing.
travel at an excessive or illegal velocity.
an air force unit smaller than a squadron.
the act of giving someone a job.
the act of troubling or annoying someone.
droop, sink, or settle from or as if from pressure or loss of tautness.
cite as an authority; resort to.
set aside or apart for a specific purpose or use.
make out of components (often in an improvising manner).
learn by repetition.
intend (something) to move towards a certain goal.
register formally as a participant or member.
write as if with print; not cursive.
shoot down, of birds.
appoint to a clerical posts.
undergo sequestration by forming a stable compound with an ion.
furnish with an opening to allow air to circulate or gas to escape.
stand with arms or forelegs raised, as if menacing.
any accounting period of 12 months.
convert the genetic information in (a strand of DNA) into a strand of RNA, especially messenger RNA.
give a thrashing to; beat hard.
immerse or be immersed in a boiling liquid, often for cooking purposes.
supply with water, as with channels or ditches or streams.
the period of time during which a contract conveying property to a person is in effect.
protect or strengthen with sandbags; stop up.
make a tour of a certain place.
the head of a city government.
take up with the tongue.
a city on the River Thames in Berkshire in southern England.
a chart or map showing the movements or progress of an object.
take away a vital or essential part of.
the quality of being inadequate or falling short of perfection.
pat or squeeze fondly or playfully, especially under the chin.
make up something artificial or untrue.
the act of swallowing.
have unlawful sex with a whore.
reverse the winding or twisting of.
reimburse or compensate (someone), as for a loss.
sharpen with a strap.
the longer of the two telegraphic signals used in Morse code.
the part of the body between the neck and the upper arm.
rain, hail, or snow hard and be very windy, often with thunder or lightning.
fill with high spirits; fill with optimism.
the terminal forced release of pressure built up during the occlusive phase of a stop consonant.
let or cause to fall in drops.
the status of being a champion.
draw advantages from.