from __future__ import division
from sys import stdin
import sys
import pickle
import io_helper
from sts import SemanticComparer
import nltk
from scipy import stats
import numpy as np
import math
from inf_content import InformationContent

ic_src = None 
ic_trg = None

if len(sys.argv) != 8 and len(sys.argv) != 10: 
    print("Incorrect number of arguments \n USAGE: python evaluator.py [shared_emb_pickled_src] [shared_emb_pickled_trg] [stopwords_src][stopwords_trg] [src_sent_path] [trg_sent_path] [gs_labels_path] [OPTIONAL: word_freqs_src_path] [OPTIONAL: word_freqs_trg_path]")
    exit()

emb_dic_src_path = sys.argv[1]
emb_dic_trg_path = sys.argv[2]
stopwords_src_path = sys.argv[3]
stopwords_trg_path = sys.argv[4]
src_sent_path = sys.argv[5]
trg_sent_path = sys.argv[6]
gs_labels_path = sys.argv[7]

if (len(sys.argv) == 10):
    ic_src_path = sys.argv[8]
    ic_trg_path = sys.argv[9]
    
    ic_src = InformationContent(ic_src_path)
    ic_trg = InformationContent(ic_trg_path)

print("Loading src embeddings...")
translated_dict = io_helper.deserialize_embeddings_dict(emb_dic_src_path)

print("Loading trg embeddings...")
emb_dict_trg = io_helper.deserialize_embeddings_dict(emb_dic_trg_path)

src_stpwrds = io_helper.load_lines_from_file(stopwords_src_path)
trg_stpwrds = io_helper.load_lines_from_file(stopwords_trg_path)

semcomp = SemanticComparer(translated_dict, emb_dict_trg, src_stpwrds, trg_stpwrds) 

src_sentences = io_helper.load_lines_from_file(src_sent_path)
trg_sentences = io_helper.load_lines_from_file(trg_sent_path)
gs = [float(x) for x in io_helper.load_lines_from_file(gs_labels_path)]

similarities_alignment = []
similarities_aggregation = []

sentence_pairs = zip(src_sentences, trg_sentences)

for sent_pair in sentence_pairs:
    print("Source language sentence: " + sent_pair[0])  
    print("Target language sentence: " + sent_pair[1])  

    simil_align = semcomp.alignment_similarity(sent_pair[0], sent_pair[1], ic_src, ic_trg)
    simil_aggreg = semcomp.aggregation_similarity(sent_pair[0], sent_pair[1], ic_src, ic_trg)
        
    print("Sim. alignment: " + str(simil_align))
    print("Sim. aggregation: " + str(simil_aggreg))

    similarities_alignment.append(0 if math.isnan(simil_align) else simil_align)
    similarities_aggregation.append(0 if math.isnan(simil_aggreg) else simil_aggreg)

min_pred_align = min(similarities_alignment)
max_pred_align = max(similarities_alignment)
min_pred_aggreg = min(similarities_aggregation)
max_pred_aggreg = max(similarities_aggregation)

mod_sims_align = [5*((x - min_pred_align)/(max_pred_align - min_pred_align)) for x in similarities_alignment]
mod_sims_aggreg = [5*((x - min_pred_aggreg)/(max_pred_aggreg - min_pred_aggreg)) for x in similarities_aggregation]


# alignment similarity performance
pearson_align = stats.pearsonr(gs, similarities_alignment)
spearman_align = stats.spearmanr(gs, similarities_alignment)

print("Alignment similarity\n----------------------------")
print("Pearson: " + str(pearson_align))
print("Spearman: " + str(spearman_align))
print()

# aggregation similarity performance
pearson_aggreg = stats.pearsonr(gs, similarities_aggregation)
spearman_aggreg = stats.spearmanr(gs, similarities_aggregation)

print("Aggregation similarity\n----------------------------")
print("Pearson: " + str(pearson_aggreg))
print("Spearman: " + str(spearman_aggreg))
print()

print("Total tokens: " + str(semcomp.total_num_tokens))
print("Not found in embeddings: " + str(semcomp.missing_from_embs))
