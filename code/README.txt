This archive contains the software that accompany the anonymous EMNLP submission
"A Poor Man's Approach to Cross-Lingual Semantic Textual Similarity" . 

The program code (Python scripts) allow training the translation matrices (tm_trainer.py) 
and running the cross-lingual STS models (evaluator.py) on evaluation datasets accompanying the same 
publication. 

TRAINING THE TRANSLATION MATRICES

In order to train the translation matrices that maps the source language embeddings to the target language 
embedding space one should execute the following command: 

python tm_trainer.py [src_lang_emb] [trg_lang_emb] [trans-pairs-path] [shared_embs_src] [shared_embs_trg]

The arguments are as follows:
1.  [src_lang_emb] is the path to the file containing the embeddings of the source language (in textual, not binary format)
2.  [trg_lang_emb] is the path to the file containing the embeddings of the target language (in textual, not binary format)
3.  [trans-pairs-path] is the path to the file containing the word translation pairs to be used as the training set for learning
the translation matrix
4.  [shared_embs_src] is the path where the model will output the source language words encoded 
in the shared embedding space
5.  [shared_embs_trg] is the path where the model will output the target language words encoded 
in the shared embedding space

EVALUATING THE CROSS-LINGUAL STS MODELS

In order to evaluate the cross-lingual models presented in the paper on cross-lingual STS test sets, one should
execute the following command: 

python evaluator.py [shared_emb_pickled_src] [shared_emb_pickled_trg] [stopwords_src][stopwords_trg] [src_sent_path] [trg_sent_path] [gs_labels_path]

The arguments are as follows:
1.  [shared_emb_pickled_src] is the path to the file containing the source language words encoded 
in the shared embedding space (should be the same as [shared_embs_src] from the command for training the translation matrix)
1.  [shared_emb_pickled_trg] is the path to the file containing the target language words encoded 
in the shared embedding space (should be the same as [shared_embs_trg] from the command for training the translation matrix)
3.  [stopwords_src] the path to the file containing the stopwords for the source language
4.  [stopwords_src] the path to the file containing the stopwords for the target language
5.  [src_sent_path] the path to the file containing the test set sentences in the source language
6.  [trg_sent_path] the path to the file containing the test set sentences in the target language
7.  [gs_labels_path] the path to the file containing the gold similarity scores for the test set



