from __future__ import division
import tensorflow as tf
from sys import stdin
import numpy as np
import random

class TranslationMatrix():
	def __init__(self, emblen_first, emblen_sec):
		
		self.source_vectors = tf.placeholder(tf.float32, shape = [None, emblen_first])
		self.target_vectors = tf.placeholder(tf.float32, shape = [None, emblen_sec])
		
		self.trans_matrix = tf.Variable(tf.random_uniform([emblen_first, emblen_sec], -1.0, 1.0))
		self.predictions_target = tf.matmul(self.source_vectors, self.trans_matrix)
		self.objective =  tf.nn.l2_loss(tf.sub(self.predictions_target, self.target_vectors))
		
		self.train_op = tf.train.AdamOptimizer(0.001).minimize(self.objective)
		
	def train(self, src_vec, trg_vec, sess):
		fd = {self.source_vectors : src_vec, self.target_vectors : trg_vec}
		sess.run(self.train_op, feed_dict = fd)
		
	def evaluate(self, src_vec, trg_vec, sess):
		fd = {self.source_vectors : src_vec, self.target_vectors : trg_vec}
		objval = sess.run(self.objective, feed_dict = fd)
		return objval;
		
	def translate(self, src_vec, sess):
		fd = {self.source_vectors : src_vec}
		objval = sess.run(self.predictions_target, feed_dict = fd)
		return objval
		
	
	