from __future__ import division
from sys import stdin
import codecs
import pickle

def load_embeddings_dict(filepath):
	words, embeddings = pickle.load(open(filepath, 'rb'))
	return dict(zip(words, embeddings))

def load_embeddings_dict_raw(filepath, limit):
    dict = {}
    cnt = 0
    with codecs.open(filepath,'r',encoding='utf8', errors='replace') as f:
        for line in f:
            try:
                cnt += 1
                if limit and cnt > limit: 
                    break
                if (cnt % 1000 == 0): 
                    print("Loading embeddings: " + str(cnt))
                if cnt > 1:
                    splt = line.split()
                    word = splt[0]
                    vec = [float(x) for x in splt[1:]]
                    dict[word] = vec
            except (IndexError, UnicodeEncodeError):
                print("Incorrect format line!")
    return dict

def deserialize_embeddings_dict(filepath):
    dict = pickle.load(open(filepath, 'rb'))
    return dict

def serialize_embeddings_dict(filepath, dict):
    f = open(filepath,'wb')
    pickle.dump(dict, f)
     
def store_embeddings_dict(filepath, dict):
	text_file = codecs.open(filepath, "w", "utf-8")
	for w in dict:
		text_file.write(w + " ")
		for i in range(len(dict[w])):
			text_file.write(str(dict[w][i]) + " ")
		text_file.write("\n")
	text_file.close()

def load_translation_pairs(filepath, embdict_src, embdict_trg):
	lines = list(open(filepath, "r").readlines())
	dataset = []; 
	for line in lines:
		spl = line.split(',')
		srcword = spl[0].strip()
		trgword = spl[1].strip(); print(srcword + " :: " + trgword); print(srcword in embdict_src); print(trgword in embdict_trg)
		

		if (" " not in srcword.strip()) and  (" " not in trgword.strip()) and (srcword in embdict_src) and (trgword in embdict_trg):
			dataset.append((srcword, trgword, embdict_src[srcword], embdict_trg[trgword])); print("Appending: " + srcword + " :: " + trgword)
	return dataset

def load_lines_from_file(filepath):
    return [x.strip() for x in list(codecs.open(filepath, "r", "utf-8").readlines())]
