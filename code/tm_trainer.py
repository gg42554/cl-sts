# -*- coding: utf-8 -*-

from __future__ import division
import tensorflow as tf
from sys import stdin
import numpy as np
import random
from translation_matrix import TranslationMatrix
import pickle
from scipy import spatial
import codecs
import io_helper
import nltk
import sys

def split_train_eval(data, ratio_eval):
	train = []
	test = []
	for d in data:
		r = random.uniform(0.0, 1.0)
		if r > ratio_eval:
			test.append(d)
		else:
			train.append(d)
	return [train, test]

def select_batch(train_data, batch_size):
	batch_train = []
	selected_indices = []
	while (len(batch_train) < batch_size):
		i = random.randint(0, len(train_data) - 1)
		if i not in selected_indices:
			selected_indices.append(i)
			batch_train.append(train_data[i])			
	return batch_train


def most_similar(embedding, emb_dict, num):
	ms = []
	for w in emb_dict:
		if len(embedding) != len(emb_dict[w]):
			print("Unaligned embedding length: " + w)
		else:
			sim = 1 - spatial.distance.cosine(embedding, emb_dict[w])
			if (len(ms) < num) or (sim > ms[-1][1]):
				ms.append((w, sim))
				ms.sort(key = lambda x: x[1], reverse = True)
				if len(ms) > num: 
					ms.pop() 
	return [ws[0] for ws in ms]
	

# parameters
train_set_rel_size = 0.8
num_iterations = 15000
batch_size = 500
one_big_batch = True
just_train = True
num_emb_limit = 500000

embs_src_pickled = False
embs_trg_pickled = False

if len(sys.argv) < 6: 
    print("Incorrect number of arguments \n USAGE: python tm_trainer.py [src_lang_emb] [trg_lang_emb] [trans-pairs-path] [shared_embs_src] [shared_embs_trg]")
    exit()

emb_dic_src_path = sys.argv[1]
emb_dic_trg_path = sys.argv[2]
trans_pairs_path = sys.argv[3]
shared_emb_src_path = sys.argv[4]
shared_emb_trg_path = sys.argv[5]

# Loading embeddings
emb_dict_src = io_helper.load_embeddings_dict(emb_dic_src_path) if embs_src_pickled else io_helper.load_embeddings_dict_raw(emb_dic_src_path, num_emb_limit)
emb_dict_trg = io_helper.load_embeddings_dict(emb_dic_trg_path) if embs_trg_pickled else io_helper.load_embeddings_dict_raw(emb_dic_trg_path, num_emb_limit)

len_emb_src = len(emb_dict_src[emb_dict_src.keys()[200]])
len_emb_trg = len(emb_dict_trg[emb_dict_trg.keys()[200]])

# loading translation pairs
all_data = 	io_helper.load_translation_pairs(trans_pairs_path, emb_dict_src, emb_dict_trg)
print("Number of translations pairs: " + str(len(all_data)))

if just_train:
	train_set_rel_size = 1.0
train_set, test_set = split_train_eval(all_data, train_set_rel_size)

train_src = np.array(zip(*train_set)[2])
train_trg = np.array(zip(*train_set)[3])

if not just_train:
	test_src = np.array(zip(*test_set)[2])
	test_trg = np.array(zip(*test_set)[3])

# initializing tf session and trans. matrix object
session = tf.InteractiveSession()
trans_mat = TranslationMatrix(len_emb_src, len_emb_trg)
session.run(tf.initialize_all_variables())

for i in range(num_iterations):
	if one_big_batch:
		trans_mat.train(train_src, train_trg, session)
	else:
		batch_train = select_batch(train_set, batch_size)
		[words_src, words_trg, vecs_src, vecs_trg] = zip(*batch_train)
	
		train_batch_src = np.array(vecs_src)
		train_batch_trg = np.array(vecs_trg)
		
		trans_mat.train(train_batch_src, train_batch_trg, session)
	
	if (i % 50 == 0):
		norm_train_error = trans_mat.evaluate(train_src, train_trg, session) / (len(train_set) * 1.0)
		if not just_train: 
			norm_test_error = trans_mat.evaluate(test_src, test_trg, session) / (len(test_set) * 1.0)
		print(i)
		print("Train error: " + str(norm_train_error))
		if not just_train: 
			print("Test error: " + str(norm_test_error) + "\n")

# Translating embeddings 
print("Creating shared embeddings space (translating source space to target space)...")
src_keys = emb_dict_src.keys()
input_vecs = np.empty(shape=(len(emb_dict_src), len_emb_src))

for i in range(len(src_keys)):
    in_vec = emb_dict_src[src_keys[i]]
    if len(in_vec) < len_emb_src:
        in_vec = np.lib.pad(in_vec, (0, len_emb_src - len(in_vec)), 'constant', constant_values=(0, 1))
    input_vecs[i] = in_vec
output_vecs = trans_mat.translate(input_vecs, session)
translated_dict = dict(zip(src_keys, output_vecs))
print("Done!")

# Exporting embeddings 
print("Storing embeddings in shared embeddings space...")

io_helper.serialize_embeddings_dict(shared_emb_src_path, translated_dict)
io_helper.serialize_embeddings_dict(shared_emb_trg_path, emb_dict_trg)


#Measuring P@1 and P@5 for translation matrices 

#test_words_src =  np.array(zip(*test_set)[0])
#test_words_trg =  np.array(zip(*test_set)[1])

#match_p1 = 0
#match_p5 = 0
#cnt_valid = 0
#for i in range(len(test_words_src)):
#    print("Test pair " + str(i+1) + "/" + str(len(test_words_src)) + ": " + test_words_src[i] + " " + test_words_trg[i])
#    if test_words_src[i] in translated_dict:
#        cnt_valid += 1
#        test_vec = translated_dict[test_words_src[i]]
#        ms = most_similar(test_vec, emb_dict_trg, 5)
#        print("Most similar: " + str(ms))
#        if (ms[0].lower() == test_words_trg[i].lower()):
#            print("P1 match: " + test_words_src[i] + " " + test_words_trg[i])
#            match_p1 += 1
#        if test_words_trg[i].lower() in [m.lower() for m in ms]:
#            print("P5 match: " + test_words_src[i] + " " + test_words_trg[i])
#            match_p5 += 1

#print(str(match_p1) + "; " + str(match_p5) + "; " + " " + str(cnt_valid))
#print(str(match_p1 / cnt_valid) + "; " + str(match_p5 / cnt_valid))