from __future__ import division
import numpy as np
import sys
import codecs
import math

class InformationContent():
    def __init__(self, filepath):
        self.freqdict = {}
        self.minfreq = sys.maxint
        self.maxfreq = 0
        self.sum = 0

        with codecs.open(filepath,'r',encoding='utf8') as f:
            cnt = 0
            for line in f:
                cnt += 1
                if cnt % 1000 == 0:
                    print("Loading inf. cont., line: " + str(cnt))
                try:
                    
                    splt = line.split()
                    word = splt[0].strip()
                    count = int(splt[1].strip())
                    
                    if count < self.minfreq:
                        self.minfreq = count
                    if count > self.maxfreq:
                        self.maxfreq = count
                    
                    self.freqdict[word] = count
                    self.sum += count

                except ValueError:
                    print("Line in the wrong format!")

        self.minic = -1.0 * math.log(self.maxfreq / self.sum)
        self.maxic = -1.0 * math.log(self.minfreq / self.sum)

    def value(self, word):
        if word in self.freqdict:
            ic = -1.0 * math.log(self.freqdict[word] / self.sum)
            normic = (ic - self.minic) / (self.maxic - self.minic)
            return normic
        else:
            return 1