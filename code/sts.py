# -*- coding: utf-8 -*-

from __future__ import division
import numpy as np
from scipy import spatial
import nltk
from munkres import Munkres, print_matrix
from sys import stdin

class SemanticComparer():
    def __init__(self, src_embeddings, trg_embeddings, src_stopwords, trg_stopwords):
        self.embs_src = src_embeddings
        self.embs_trg = trg_embeddings
        self.src_stp = src_stopwords
        self.trg_stp = trg_stopwords
        self.punctuation = [".", ",", "!", ":", "?", ";", "-", ")", "(", "[", "]", "{", "}", "...", "/", "\\", u"``", "''", "\"", "'"]
        self.total_num_tokens = 0.0;
        self.missing_from_embs = 0.0;
    
    def select_eligible_tokens(self, src_lang_sent, trg_lang_sent):
        src_tokens = [x.strip().lower() for x in nltk.word_tokenize(src_lang_sent) if x.strip().lower() not in self.src_stp and x.strip().lower() not in self.punctuation]
        trg_tokens = [x.strip().lower() for x in nltk.word_tokenize(trg_lang_sent) if x.strip().lower() not in self.trg_stp and x.strip().lower() not in self.punctuation]
        
        for i in range(len(trg_tokens)):
            pcs = [p for p in self.punctuation if trg_tokens[i].endswith(p)]
            if (len(pcs) > 0):
                trg_tokens[i] = trg_tokens[i][:-1]

        return [src_tokens, trg_tokens]

    def aggregation_similarity(self, src_lang_sent, trg_lang_sent, ic_src = None, ic_trg = None):
        src_tokens, trg_tokens = self.select_eligible_tokens(src_lang_sent, trg_lang_sent)
        self.total_num_tokens += len(src_tokens) + len(trg_tokens)

        emb_size_src = len(self.embs_src[self.embs_src.keys()[0]])
        vec_src =np.zeros(emb_size_src)
        for tok in src_tokens:
            if tok.lower() in self.embs_src:
                vec_src += np.multiply(ic_src.value(tok.lower()) if ic_src is not None else 1.0, self.embs_src[tok.lower()])
            else:
                self.missing_from_embs += 1
        
        emb_size_trg = len(self.embs_trg[self.embs_trg.keys()[0]])
        vec_trg =np.zeros(emb_size_trg)
        for tok in trg_tokens:
            if tok.lower() in self.embs_trg:
                vec_trg += np.multiply(ic_trg.value(tok.lower()) if ic_trg is not None else 1.0, self.embs_trg[tok.lower()])
            else:
                self.missing_from_embs += 1
        
        vec_src = np.multiply(1.0 /np.linalg.norm(vec_src), vec_src)
        vec_trg = np.multiply(1.0 /np.linalg.norm(vec_src), vec_trg)

        if np.isnan(np.min(vec_src)) or np.isnan(np.min(vec_trg)): sim = 0

        else: sim = 1 - spatial.distance.cosine(vec_src, vec_trg)
        return sim

    def alignment_similarity(self, src_lang_sent, trg_lang_sent, ic_src = None, ic_trg = None):
        src_tokens, trg_tokens = self.select_eligible_tokens(src_lang_sent, trg_lang_sent)
        self.total_num_tokens += len(src_tokens) + len(trg_tokens)

        if len(src_tokens) == 0 or len(trg_tokens) == 0:
            return 0

        # bipartite matching (Hungarian algorithm)
        matrix = []
        maxlen = max(len(src_tokens), len(trg_tokens))

        if ic_src is not None:
            sum_ic_src = 0
            sum_ic_trg = 0

        for i in range(maxlen):
            if i < len(src_tokens):
                if ic_src is not None:
                    sum_ic_src += ic_src.value(src_tokens[i].lower())

                if src_tokens[i].lower() not in self.embs_src:
                    self.missing_from_embs += 1

                row = []
                for j in range(maxlen): 
                    sim = 0
                    if j < len(trg_tokens):
                        if i == 0 and ic_trg is not None:
                            sum_ic_trg += ic_trg.value(trg_tokens[j].lower())

                        if trg_tokens[j].lower() not in self.embs_trg:
                            self.missing_from_embs += 1.0/len(src_tokens)

                        if src_tokens[i].lower() == trg_tokens[j].lower():
                            sim = 1
                        elif (src_tokens[i].lower() in self.embs_src) and (trg_tokens[j].lower() in self.embs_trg):
                            emb1 = self.embs_src[src_tokens[i].lower()]
                            emb2 = self.embs_trg[trg_tokens[j].lower()]
                            sim = 1 - spatial.distance.cosine(emb1, emb2)
                        #else:
                            #if src_tokens[i].lower() not in self.embs_src:
                            #    print(src_tokens[i].lower() + " not in EMBS src")
                            #if trg_tokens[j].lower() not in self.embs_trg:
                            #    print(trg_tokens[j].lower() + " not in EMBS trg")

                    cost = 1.0 - sim;
                    row.append(cost)
                matrix.append(row)
            else:
                matrix.append([1]*maxlen)
        
        m = Munkres()
        index_pairs = m.compute(matrix)

        total_sim = 0.0
        for row, column in index_pairs:
            first_word = src_tokens[row] if row < len(src_tokens) else "--"
            second_word = trg_tokens[column] if column < len(trg_tokens) else "--"

            value = matrix[row][column]
            raw_sim = 1 - value
            print(first_word + "::" + second_word + ", SIM: " + str(1 - value))
            
            if ic_src is not None:
                ic_first = ic_src.value(first_word.lower())
                ic_second = ic_src.value(second_word.lower())
                ic_pair = max(ic_first, ic_second) #(ic_first + ic_second) / 2
                print(first_word + "::" + second_word + ", IC: " + str(ic_pair))
                total_sim += raw_sim * ic_pair
            else:
                total_sim += raw_sim

        if ic_src is None: 
            prec = total_sim / (len(src_tokens)*1.0)
            rec = total_sim / (len(trg_tokens)*1.0)
        else:
            prec = total_sim / sum_ic_src
            rec = total_sim / sum_ic_trg
 
        return ((prec + rec)/2.0)
        #return (2*prec*rec) / ((prec + rec))        
        